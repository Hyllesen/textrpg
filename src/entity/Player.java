package entity;

import java.util.Scanner;

import Room.Room;
import Room.RoomList;

public class Player {
	
	private int hp;
	private int gold;
	private int inventory;
	private Room currentRoom;
	private String name;
	private RoomList roomList = new RoomList();
	Scanner scan = new Scanner(System.in);


	public Player(int hp, int gold, int inventory, Room currentRoom, String name) {
		this.hp = hp;
		this.gold = gold;
		this.inventory = inventory;
		this.currentRoom = currentRoom;
	}

	public void moveOption() {
		
	}

	public void giveTurn() {
		System.out.println(name + "what action do you choose?");
//		showAvalibleActions();
		String choice = scan.nextLine();
	}


//	private void showAvalibleActions() {
//		System.out.println("1. Move (" + showAvalibleMoves() + ")");	
//	}


	public void showAvailableMoves() {
		int room = currentRoom.moveDialogue();
		currentRoom = roomList.getRoom(room);
	}
	
	
	
}
