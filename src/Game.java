import entity.Player;


public class Game {
	
	private Player player;
	
	public void init() {
		player = new Player(100, 0, 1, 0);
		start();
	}
	
	public void start() {
		System.out.println("Welcome to Dungeon Crawler v. 0.01");
	}

}
