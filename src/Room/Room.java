package Room;
import java.util.Scanner;

import entity.Entity;


public class Room {
	private int[] roomsNearby = new int[4]; 
	private Entity entity;
	private String description;
	private Scanner scan = new Scanner(System.in);
	
	/**
	 * 
	 * @param north
	 * @param south
	 * @param east
	 * @param west
	 */
	public Room(int north, int south, int east, int west, String description) {
		roomsNearby[0] = north;
		roomsNearby[1] = south;
		roomsNearby[2] = west;
		roomsNearby[3] = east;
		this.description = description;
	}
	
	public int moveDialogue() {	
		String choice;
		int roomIndex = 0;
		System.out.println("You are in: " + description);
		
		System.out.println("Available moves: ");
			if(roomsNearby[0] != -1) {
				System.out.println("N");
			}
			if(roomsNearby[1] != -1) {
				System.out.println("S");
			}
			if(roomsNearby[3] != -1) {
				System.out.println("E");
			}
			if(roomsNearby[2] != -1) {
				System.out.println("W");
			}
	
		System.out.println("Please choose where you want to move to:");
		choice = scan.nextLine();
		choice = choice.toUpperCase();
		if(choice.trim().equals("S")) {
			roomIndex = roomsNearby[1];
		} else if(choice.trim().equals("N")) {
			roomIndex = roomsNearby[0];
		} else if(choice.trim().equals("E")) {
			roomIndex = roomsNearby[3];
		} else if(choice.trim().equals("W")) {
			roomIndex = roomsNearby[2];
		}
		return roomIndex;
		
	}	
	
}


