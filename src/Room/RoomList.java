package Room;

import java.util.ArrayList;
import java.util.List;

public class RoomList {
	
	Room[] room = new Room[7];
	List<Room> roomList = new ArrayList<Room>();

	
	public RoomList() {
		room[0] = new Room(1,3,2,4, "Starting room");
		room[1] = new Room(-1,0,-1,-1, "Room with shiva god");
		room[2] = new Room(-1,-1,-1,0, "Room with gold chest");
		room[3] = new Room(0,-1,5,-1, "Room with guy who knows");
		room[4] = new Room(-1,-1,0,6, "Forest");
		room[5] = new Room(-1,-1,-1,3, "Empty room");
		room[6] = new Room(-1,-1,4,-1, "Goat land");
	}


	public Room getRoom(int roomIndex) {
		return room[roomIndex];
	}
	
}
